const userToken = 'UserToken'
const loginUser = 'LoginUser'
const sidebarMenu = []

export default {
    userToken,
    loginUser,
    sidebarMenu
}