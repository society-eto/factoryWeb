import request from '@/utils/request'

/**
 * 用户登录
 * @param {*} data 
 */
export function login(data) {
    return request({
        url: '/rest/user/login',
        method: 'post',
        data
    })
}

/**
 * 退出登录
 */
export function logout() {
    return request({
        url: '/rest/user/loginOut',
        method: 'get'
    })
}
/**
 * 获取用菜单
 * @param {*} data 
 */
export function queryUserMenus(data) {
    return request({
        url: '/rest/user/queryUserMenus',
        method: 'post',
        data
    })
}