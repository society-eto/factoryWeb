import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
    {
        path: '/login',
        component: () => import('@/views/login/index'),
        hidden: true
    },
    {
        path: '/resetPassword',
        component: () => import('@/views/login/resetPassword'),
        hidden: true
    },
    {
        path: '/404',
        component: () => import('@/views/404'),
        hidden: true
    },

    {
        path: '/register',
        component: () => import('@/views/login/register'),
        hidden: true
    },
    {
        path: '/',
        component: Layout,
        redirect: '/index',
        children: [{
            path: 'index',
            name: 'index',
            component: () => import('@/views/device/index'),
            meta: { title: '首页', icon: 'dashboard' }
        }]
    },
    {
        path: '/device',
        component: Layout,
        children: [{
            path: 'index',
            name: 'device',
            component: () => import('@/views/device/index'),
            meta: { title: '设备控制台', icon: 'dashboard' }
        }]
    },
    {
        path: '/datacenter',
        component: Layout,
        children: [
            {
                path: 'member',
                component: () => import('@/views/datacenter/index'),
                children:[{
                    path: 'member',
                    name: 'member',
                    component: () => import('@/views/datacenter/member/member'),
                    meta: { title: '会员数据', icon: 'form' }
                },
                {
                    path: 'memberFace',
                    name: 'memberFace',
                    component: () => import('@/views/datacenter/member/memberFace'),
                    meta: { title: '人脸库数据', icon: 'form' }
                }
                ,
                {
                    path: 'index',
                    name: 'index',
                    component: () => import('@/views/datacenter/member/index'),
                    meta: { title: '统计数据', icon: 'form' }
                }]
            },
        ]
    },
    {
        path: '/alarm',
        component: Layout,
        children: [{
            path: 'index',
            name: 'alarm',
            component: () => import('@/views/alarm/index'),
            meta: { title: '告警数据', icon: 'dashboard' }
        }]
    },
    {
        path: '/capture',
        component: Layout,
        children: [{
            path: 'index',
            name: 'capture',
            component: () => import('@/views/capture/index'),
            meta: { title: '人脸抓拍', icon: 'dashboard' }
        }]
    },
    { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
    const newRouter = createRouter()
    router.matcher = newRouter.matcher // reset router
}

export default router
