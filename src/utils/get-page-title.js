import defaultSettings from '@/settings'

const title = defaultSettings.title || '慧眼云'

export default function getPageTitle(pageTitle) {
    if (pageTitle) {
        return `${pageTitle} - ${title}`
    }
    return `${title}`
}
