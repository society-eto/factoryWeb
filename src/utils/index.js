/**
 * Created by PanJiaChen on 16/11/18.
 */

/**
 * Parse the time to string
 * @param {(Object|string|number)} time
 * @param {string} cFormat
 * @returns {string}
 */
import moment from 'moment'
export function parseTime(time, cFormat) {
    if (arguments.length === 0) {
        return null
    }
    const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
    let date
    if (typeof time === 'object') {
        date = time
    } else {
        if ((typeof time === 'string') && (/^[0-9]+$/.test(time))) {
            time = parseInt(time)
        }
        if ((typeof time === 'number') && (time.toString().length === 10)) {
            time = time * 1000
        }
        date = new Date(time)
    }
    const formatObj = {
        y: date.getFullYear(),
        m: date.getMonth() + 1,
        d: date.getDate(),
        h: date.getHours(),
        i: date.getMinutes(),
        s: date.getSeconds(),
        a: date.getDay()
    }
    const time_str = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
        let value = formatObj[key]
        // Note: getDay() returns 0 on Sunday
        if (key === 'a') { return ['日', '一', '二', '三', '四', '五', '六'][value] }
        if (result.length > 0 && value < 10) {
            value = '0' + value
        }
        return value || 0
    })
    return time_str
}

/**
 * @param {number} time
 * @param {string} option
 * @returns {string}
 */
export function formatTime(time, option) {
    if (('' + time).length === 10) {
        time = parseInt(time) * 1000
    } else {
        time = +time
    }
    const d = new Date(time)
    const now = Date.now()

    const diff = (now - d) / 1000

    if (diff < 30) {
        return '刚刚'
    } else if (diff < 3600) {
        // less 1 hour
        return Math.ceil(diff / 60) + '分钟前'
    } else if (diff < 3600 * 24) {
        return Math.ceil(diff / 3600) + '小时前'
    } else if (diff < 3600 * 24 * 2) {
        return '1天前'
    }
    if (option) {
        return parseTime(time, option)
    } else {
        return (
            d.getMonth() +
            1 +
            '月' +
            d.getDate() +
            '日' +
            d.getHours() +
            '时' +
            d.getMinutes() +
            '分'
        )
    }
}

/**
 * @param {string} url
 * @returns {Object}
 */
export function param2Obj(url) {
    const search = url.split('?')[1]
    if (!search) {
        return {}
    }
    return JSON.parse(
        '{"' +
        decodeURIComponent(search)
            .replace(/"/g, '\\"')
            .replace(/&/g, '","')
            .replace(/=/g, '":"')
            .replace(/\+/g, ' ') +
        '"}'
    )
}
export function formatDate(time, formatPattern) {
    const date = 'YYYY-MM-DD'
    return moment(time).format(formatPattern || date)
}

/**
 * 测试网络是否通畅
 * @param {} ip 
 */
export function ping(ip, success, error) {
    var img = new Image();
    var start = new Date().getTime();
    var flag = false;
    var isCloseWifi = true;
    var hasFinish = false;
    img.onload = function () {
        if (!hasFinish) {
            flag = true;
            hasFinish = true;
            img.src = 'X:\\';
            console.log('Ping ' + ip + ' success. ');
            success();
            clearTimeout(t1);
            clearTimeout(t2);
        }
    };
    img.onerror = function () {
        if (!hasFinish) {
            if (!isCloseWifi) {
                flag = true;
                img.src = 'X:\\';
                console.log('Ping ' + ip + ' success. ');
                success();
                clearTimeout(t1);
                clearTimeout(t2);
            } else {
                console.log('network is not working!');
                error();
                clearTimeout(t1);
                clearTimeout(t2);
            }
            hasFinish = true;
        }
    };

    var t1 = setTimeout(function () {
        isCloseWifi = false;
        console.log('network is working, start ping...');
    }, 2);



    img.src = 'http://' + ip + '/' + start;

    var t2 = setTimeout(function () {
        if (!flag) {
            hasFinish = true;
            img.src = 'X://';
            flag = false;
            console.log('Ping ' + ip + ' fail. ');
            error();
            clearTimeout(t1);
            clearTimeout(t2);
        }
    }, 1500);
}

/**
 * 检测权限
 * @param {} auth 
 */
export function hasAuth(auth){

    let menu = JSON.parse(window.localStorage.getItem('user-menu'))
    for(let i in menu){
        let item = menu[i]
        if(item.id == auth){
            return true
        }
        if(item.children){
            for(let j in item.children){
                if(item.children[j].id == auth){
                    return true
                }
            }
        }
    }
    return false
}

export default { formatDate, param2Obj, formatTime, ping, hasAuth }
